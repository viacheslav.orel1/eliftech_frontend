import { put, call, takeEvery } from 'redux-saga/effects';

import {
	setBank,
	setBanks,
	setLoading,
} from '../../actions/bankActions/actions';
import { bankModel } from '../../utils/api';
import {
	GET_ALL_BANK_SAGA,
	GET_BANK_SAGA,
	CREATE_BANK_SAGA,
	UPDATE_BANK_SAGA,
	DELETE_BANK_SAGA,
} from '../../utils/constants';

function* fetchBanks() {
	try {
		yield put(setLoading(true));
		const { banks } = yield call(bankModel.getBank);
		yield put(setLoading(false));
		yield put(setBanks(banks));
	} catch (error) {
		console.error(error);
	}
}

function* fetchGetBankById(id) {
	try {
		yield put(setLoading(true));
		const { bankData } = yield call(bankModel.getBank(id));
		yield put(setLoading(false));
		yield put(setBank(bankData));
	} catch (error) {
		console.error(error);
	}
}

function* fetchUpdateBankById(bankData) {
	try {
		yield call(bankModel.changeBank(bankData));
	} catch (error) {
		console.error(error);
	}
}

function* fetchDeleteBankById(id) {
	try {
		yield call(bankModel.deleteBank(id));
	} catch (error) {
		console.error(error);
	}
}

export function* watchBanks() {
	yield takeEvery(GET_ALL_BANK_SAGA, fetchBanks);
	yield takeEvery(GET_BANK_SAGA, fetchGetBankById);
	yield takeEvery(UPDATE_BANK_SAGA, fetchUpdateBankById);
	yield takeEvery(DELETE_BANK_SAGA, fetchDeleteBankById);
}
