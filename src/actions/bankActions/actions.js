import {
	GET_ALL_BANK_SAGA,
	CREATE_BANK_SAGA,
	UPDATE_USER_SAGA,
	DELETE_BANK_SAGA,
	SET_LOADING,
	SET_BANKS,
	SET_BANK,
} from '../../utils/constants';

export const getAllBanks = () => ({
	type: GET_ALL_BANK_SAGA,
});

export const createBank = bank => ({
	type: CREATE_BANK_SAGA,
	payload: bank,
});

export const updateBank = user => ({
	type: UPDATE_USER_SAGA,
	payload: user,
});

export const deleteBank = id => ({
	type: DELETE_BANK_SAGA,
	payload: id,
});

export const setLoading = loading => ({
	type: SET_LOADING,
	payload: loading,
});

export const setBanks = banks => ({
	type: SET_BANKS,
	payload: banks,
});

export const setCurrentBank = bank => ({
	type: SET_BANK,
	payload: bank,
});
