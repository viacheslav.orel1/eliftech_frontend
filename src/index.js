import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter } from 'react-router-dom';
import { ThemeProvider, CssBaseline } from '@mui/material';

import App from './components/App/App';

ReactDOM.render(
	<ThemeProvider>
		<BrowserRouter>
			<CssBaseline />
			<App />
		</BrowserRouter>
	</ThemeProvider>,
	document.getElementById('root')
);
