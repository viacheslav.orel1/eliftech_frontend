import {
	DELETE_BANK,
	RESET_CURRENT_BANK,
	SET_BANKS,
	SET_CURRENT_BANK,
} from '../../utils/constants';

const initialState = {
	banks: [],
	currentBank: {
		id: 0,
		name: '',
		rate: 10,
		maxLoan: 300000,
		minDownPaymentRate: 20,
		maxLoanTerm: 60,
	},
	loading: false,
};

const bankReducer = (state = initialState, { type, payload }) => {
	switch (type) {
		case SET_BANKS:
			const banks = payload;
			return { ...state, banks };
		default:
			return false;
	}
};

export default bankReducer;
