const routes = [
	{
		name: 'Home',
		value: '/',
	},
	{
		name: 'Banks',
		value: '/banks',
	},
	{
		name: 'Calculation',
		value: '/calculation',
	},
];

export default routes;
