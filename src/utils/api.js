import axios from 'axios';

const instance = axios.create({
	baseURL: 'http://localhost:3000',
});

instance.interceptors.response.use(
	response => response.data,
	error => Promise.reject(error.response)
);

const bankModel = {
	async getBanks() {
		return instance.get('/api/bank');
	},

	async createBank(bankData) {
		return instance.post('/api/bank', bankData);
	},

	async deleteBank(id) {
		return instance.delete(`/api/bank/${id}`);
	},

	async changeBank(bankData) {
		return instance.put(`/api/bank/${bankData.id}`, bankData);
	},
};

export { bankModel };
