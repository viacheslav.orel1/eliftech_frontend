function isEmpty(value) {
	return value.length === 0;
}

function isFormValid(error) {
	return !Object.values(error).some(error => error);
}

const bankValidators = {
	name: value => (isEmpty(value) ? 'Must not be empty' : undefined),
	rate: value => {
		const number = parseInt(value);
		return number < 1 || number > 100 ? 'Must be 0 to 101' : undefined;
	},
	minDownPaymentRate: value => {
		const number = parseInt(value);
		return number < 10 ? 'Must be grate 9' : undefined;
	},
	maxLoanTerm: value => {
		const number = parseInt(value);
		return number > 60 ? 'Must be less 60' : undefined;
	},
};

export { isFormValid, isEmpty, bankValidators };
