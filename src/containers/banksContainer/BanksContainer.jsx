import {
	CircularProgress,
	Container,
	Button,
	Grid,
	Dialog,
} from '@mui/material';
import { Box } from '@mui/system';
import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';

import Bank from '../../components/Bank/Bank';
import { bindActionCreators } from 'redux';
import {
	createBank,
	deleteBank,
	getAllBanks,
	setCurrentBank,
	updateBank,
} from '../../actions/bankActions/actions';
import { connect } from 'react-redux';

const mapStateToProps = state => ({
	banks: state.banksReducer.banks,
	loading: state.banksReducer.loading,
	currentBank: state.banksReducer.currentBank,
});

const mapDispatchToProps = dispatch => {
	return bindActionCreators(
		{
			onGetBanks: getAllBanks,
			onSetCurrentBank: setCurrentBank,
			onCreateBank: createBank,
			onUpdateBank: updateBank,
			onDeleteBank: deleteBank,
		},
		dispatch
	);
};

function BanksContainer({
	loading,
	banks,
	onGetBanks,
	onCreateBank,
	onEditBank,
	onDeleteBank,
	onSetCurrentBank,
}) {
	const [open, setOpen] = useState(false);

	useEffect(() => {
		onGetBanks();
	}, [onGetBanks]);

	const openDialog = () => {
		setOpen(true);
	};

	const closeDialog = () => {
		setOpen(false);
	};

	const editBank = bank => {
		onSetCurrentBank(bank);
		openDialog();
	};

	const deleteBank = (...props) => {
		onDeleteBank(props);
	};

	const addBankButton = () => {
		openDialog();
	};

	const addOrEditBankHandle = bankData => {
		const normalizedBank = {
			name: bankData.name,
			rate: bankData.rate,
			maxLoan: bankData.maxLoan,
			minDownPaymentRate: bankData.minDownPaymentRate,
			maxLoanTerm: bankData.maxLoan,
			id: bankData.id,
		};

		return bankData.id
			? onEditBank(normalizedBank)
			: onCreateBank(normalizedBank);
	};

	return (
		<Container>
			{loading ? (
				<Box display='flex' justifyContent='center'>
					<CircularProgress size={150} m='auto' />
				</Box>
			) : (
				<>
					<Grid container spacing={2}>
						{banks.map(bank => (
							<Grid item xs={4}>
								<Bank
									id={bank.id}
									name={bank.name}
									minDownPaymentRate={bank.minDownPaymentRate}
									maxLoan={bank.maxLoan}
									maxLoanTerm={bank.maxLoanTerm}
									onEdit={() => onEditBank(bank)}
									onDelete={deleteBank}
								/>
							</Grid>
						))}
						<Grid item xs={12}>
							<Button
								onClick={addBankButton}
								variant='contained'
								color='primary'
							>
								Add new bank
							</Button>
						</Grid>
					</Grid>
					<Dialog open={open} onClose={closeDialog}>
						`
					</Dialog>
				</>
			)}
		</Container>
	);
}

BanksContainer.propTypes = {
	banks: PropTypes.arrayOf(PropTypes.object),
	loading: PropTypes.bool,
};

export const BankContainerRaw = BanksContainer;
export default connect(mapStateToProps, mapDispatchToProps)(BanksContainer);
