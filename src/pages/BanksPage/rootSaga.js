import { all, call } from 'redux-saga/effects';
import { watchBank } from '../../sagas/banksSagas/getBanksSagas';

export default function* rootSaga() {
	yield all([call(watchBank)]);
}
