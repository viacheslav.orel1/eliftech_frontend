import { applyMiddleware, combineReducers, compose, createStore } from 'redux';
import createSagaMiddleware from '@redux-saga/core';

import bankReducer from '../../reducer/bankReducer/reducer';
import bankSagas from './rootSaga';

const reducer = combineReducers({ bankReducer });
const sagaMiddleware = createSagaMiddleware();

export default function setStore() {
	const composeEnhancers =
		window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
	const store = createStore(
		reducer,
		composeEnhancers(applyMiddleware(sagaMiddleware))
	);
	sagaMiddleware.run(bankSagas);

	return store;
}
