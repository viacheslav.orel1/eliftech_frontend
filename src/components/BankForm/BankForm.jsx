import React, { useState } from 'react';
import { Paper, Grid, TextField } from '@mui/material';
import PropTypes from 'prop-types';

import { bankValidators, isFormValid } from '../../utils/validation';

const initialValue = {
	name: 'Name',
	rate: 10,
	maxLoan: 300000,
	minDownPaymentRate: 20,
	maxLoanTerm: 60,
};

function BankForm({ onClose, onSubmit, ...bankData }) {
	const [bankState, setBank] = useState(bankData);
	const [errors, setErrors] = useState();

	const handleSubmit = event => {
		event.preventDefault();
		const validationError = [];

		Object.entries(bankState).forEach(([key, value]) => {
			if (typeof value === 'string') {
				validationError[key] =
					bankValidators[key] && bankValidators[key](value);
			}
		});

		if (isFormValid(validationError)) {
			onSubmit(bankState);
			setErrors({});
			setBank(initialValue);
		} else {
			setErrors(validationError);
		}
	};

	const handleChange = event => {
		setBank(prevValues => ({
			...prevValues,
			[event.target.name]: event.target.value.trim(),
		}));
	};

	const handleCancel = () => {
		setBank(initialValue);
		setErrors({});
		onClose();
	};

	return (
		<Paper elevation={2}>
			<form onSubmit={handleSubmit}>
				<Grid container alignItems='center' spacing={2}>
					<Grid item xs={12}>
						<TextField
							name='name'
							type='text'
							variant='outlined'
							size='small'
							label='Bank Name'
							fullWidth
							value={bankState.name}
							onChange={handleChange}
							helperText={errors.name}
							error={!!errors.name}
						/>
					</Grid>
					<Grid item xs={12} sm={4}>
						<TextField
							name='rate'
							type='number'
							variant='outlined'
							size='small'
							label='Interest rate'
							fullWidth
							value={bankState.rate}
							onChange={handleChange}
							helperText={errors.name}
							error={!!errors.name}
						/>
					</Grid>
					<Grid item xs={12} sm={4}>
						<TextField
							name='maxLoan'
							type='number'
							variant='outlined'
							size='small'
							label='Maximum loan'
							fullWidth
							value={bankState.maxLoan}
							onChange={handleChange}
							helperText={errors.name}
							error={!!errors.name}
						/>
					</Grid>
					<Grid item xs={12} sm={4}>
						<TextField name='test' />
						<TextField
							name='minDownPaymentRate'
							type='number'
							variant='outlined'
							size='small'
							label='Minimum down payment rate'
							fullWidth
							value={bankState.minDownPaymentRate}
							onChange={handleChange}
							helperText={errors.name}
							error={!!errors.name}
						/>
					</Grid>
					<Grid item xs={12} sm={4}>
						<TextField
							name='maxLoanTerm'
							type='number'
							variant='outlined'
							size='small'
							label='Loan term'
							fullWidth
							value={bankState.maxLoanTerm}
							onChange={handleChange}
							helperText={errors.name}
							error={!!errors.name}
						/>
					</Grid>
				</Grid>
				<Grid item container xs={12} spacing={2} justify='center'>
					<Grid item>
						<Button color='primary' variant='contained' type='submit'>
							Save
						</Button>
					</Grid>
					<Grid item>
						<Button variant='contained' onClick={handleCancel}>
							Cancel
						</Button>
					</Grid>
				</Grid>
			</form>
		</Paper>
	);
}

BankForm.propTypes = {
	onClose: PropTypes.func,
	onSubmit: PropTypes.func,
};

BankForm.defaultProps = { ...initialValue };

export default BankForm;
