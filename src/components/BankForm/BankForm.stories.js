import React from 'react';
import BankForm from './BankForm';

const stories = {
	title: 'ElifTech/BankForm',
	component: BankForm,
};

export default stories;

const Template = args => <BankForm {...args} />;

export const NewBank = Template.bind({});
NewBank.args = {
	name: 'Some Bank',
	rate: 10,
	maxLoan: 300000,
	minDownPaymentRate: 20,
	maxLoanTerm: 36,
};
