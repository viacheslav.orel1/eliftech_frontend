import React from 'react';
import PropTypes from 'prop-types';
import {
	Card,
	CardContent,
	CardActions,
	Typography,
	ButtonGroup,
	Button,
	Box,
} from '@mui/material';

function Bank({
	id,
	name,
	rate,
	maxLoan,
	minDownPaymentRate,
	maxLoanTerm,
	onView,
	onEdit,
	onDelete,
}) {
	return (
		<Card>
			<CardContent>
				<Typography variant='h5' component='div'>
					{name}
				</Typography>
				<Typography variant='body2'>Interest rate - {rate}%</Typography>
				<Typography variant='body2'>Maximum loan - {maxLoan}$</Typography>
				<Typography variant='body2'>
					Minimum down payment rate - {minDownPaymentRate}%
				</Typography>
				<Typography term variant='body2'>
					Loan term - {maxLoanTerm} months
				</Typography>
			</CardContent>
			<CardActions>
				<Box sx={{ display: 'flex', alignContent: 'center' }}>
					<ButtonGroup variant='text'>
						<Button onClick={onView}>View</Button>
						<Button onClick={onEdit}>Edit</Button>
						<Button onClick={onDelete} color='error'>
							Remove
						</Button>
					</ButtonGroup>
				</Box>
			</CardActions>
		</Card>
	);
}

Bank.propTypes = {
	id: PropTypes.number.isRequired,
	name: PropTypes.string.isRequired,
	rate: PropTypes.number.isRequired,
	maxLoan: PropTypes.number.isRequired,
	minDownPaymentRate: PropTypes.number.isRequired,
	maxLoanTerm: PropTypes.number,
	onView: PropTypes.func,
	onEdit: PropTypes.func,
	onDelete: PropTypes.func,
};

export default Bank;
