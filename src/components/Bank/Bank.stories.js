import React from 'react';
import Bank from './Bank';

const stories = {
	title: 'ElifTech/Bank',
	component: Bank,
};

export default stories;

const Template = args => <Bank {...args} />;

export const SomeBank = Template.bind({});
SomeBank.args = {
	id: 1,
	name: 'Some Bank',
	rate: 10,
	maxLoan: 300000,
	minDownPaymentRate: 20,
	maxLoanTerm: 36,
};
